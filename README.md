<div align="center">

 <a href="https://gitlab.com/wardix-public/wardix-localization/-/blob/main/LICENSE">
   <img src="https://img.shields.io/github/license/whilein/nmslib">
 </a>

 ***
Данный репозиторий используется для мульти-язяковой системы с проекта **Wardix Project (wardix.ml)**
</div>

---
## Обратная связь
Если у Вас есть какие-то предложения или желание в помощи по переводу, 
то можете обратиться к нам через указанные контакты:

* **[Discord](https://discord.com/invite/6YUf4RhQCU)**
* **[ВКонтакте](https://vk.me/wardixnw)**
